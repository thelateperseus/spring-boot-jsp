# Spring Boot JSP Sample

Converted existing sample to Gradle: https://github.com/spring-projects/spring-boot/tree/master/spring-boot-samples/spring-boot-sample-web-jsp

To run locally:

* Clone the repo: https://bitbucket.org/thelateperseus/spring-boot-jsp
* Run `gradlew eclipse`
* Import the existing project into Eclipse
* Right-click the project and select *Run As* -> *Run on Server* and create a new Tomcat server
* Browse to http://localhost:8080/spring-boot-jsp/?message=Hi
